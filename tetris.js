   //////////////////////////////////////////////////////////////////////////////////////
  /////                 JAVA SCRIPT TETRIS (ES6) v0.3                              /////
 /////                             Autor:Ivan Grga                                /////
//////////////////////////////////////////////////////////////////////////////////////

//BEGINING OF PRIVATE SCOPE <-- all happens inside one function so nothing is declared on global scope
(function () {
    
	// USE STRICT DECLARED
    'use strict';

	  //////////////////////////////////////////////////////////////////////////////////
	 /////                              PRIMARY CLASSES                           /////
	//////////////////////////////////////////////////////////////////////////////////

	//ALL CLASSES CREATED AS CLASES NOT AS OBJECT TO ENABLE EASY EXTENSION IF NEEDED!
	
	class Part
	{
		constructor()
		{
			this.x;
			this.y;
		}
	} 

	class Position extends Part
	{

	} 

	class gridPart
	{
		constructor()
		{
			this.x;
			this.y;
			this.fill;
		}
	}
	
	//Defines size of canvas based on screan size.
	class gameSize
	{
		constructor()
		{
			this.gameHeight=window.innerHeight*0.9;
			this.gameUnit=this.gameHeight/28;
			this.gameWidth=this.gameUnit*12;
			//DEBUG:
			//console.log("unt:"+this.gameUnit);
		}
	}

	  //////////////////////////////////////////////////////////////////////////////////
	 /////                              PIECE CLASS                               /////
	//////////////////////////////////////////////////////////////////////////////////

	class Piece
	{
		constructor()
		{
			this.type;
			this.rotation;
			this.position=[];
			this.part=[];

			for(var i=0;i<4;i++)
				this.part[i]=new Part();

			for(var i=0;i<4;i++)
				this.position[i]=new Position();
		}

		setPiece(type)
		{
			// type
			this.type=type;
			//setting position of central part
			this.part[0].x=0;
			this.part[0].y=0;
			// type 0 (I)
			if(this.type==0)
			{
				// 1
				this.part[1].x=this.part[0].x;
				this.part[1].y=this.part[0].y-1;
				// 2
				this.part[2].x=this.part[0].x;
				this.part[2].y=this.part[0].y-2;
				// 3
				this.part[3].x=this.part[0].x;
				this.part[3].y=this.part[0].y+1; 
			}
			// type 1 (BOX)
			if(this.type==1)
			{
				// 1
				this.part[1].x=this.part[0].x;
				this.part[1].y=this.part[0].y-1;
				// 2
				this.part[2].x=this.part[0].x+1;
				this.part[2].y=this.part[0].y-1;
				// 3
				this.part[3].x=this.part[0].x+1;
				this.part[3].y=this.part[0].y; 
			}
			// type 2 (L)
			if(this.type==2)
			{
				// 1
				this.part[1].x=this.part[0].x;
				this.part[1].y=this.part[0].y-1;
				// 2
				this.part[2].x=this.part[0].x;
				this.part[2].y=this.part[0].y+1;
				// 3
				this.part[3].x=this.part[0].x+1;
				this.part[3].y=this.part[0].y+1; 
			}
			// type 3 (REVERSE L)
			if(this.type==3)
			{
				// 1
				this.part[1].x=this.part[0].x;
				this.part[1].y=this.part[0].y-1;
				// 2
				this.part[2].x=this.part[0].x;
				this.part[2].y=this.part[0].y+1;
				// 3
				this.part[3].x=this.part[0].x-1;
				this.part[3].y=this.part[0].y+1; 
			}
			// type 4 (T)
			if(this.type==4)
			{
				// 1
				this.part[1].x=this.part[0].x-1;
				this.part[1].y=this.part[0].y;
				// 2
				this.part[2].x=this.part[0].x+1;
				this.part[2].y=this.part[0].y;
				// 3
				this.part[3].x=this.part[0].x;
				this.part[3].y=this.part[0].y-1; 
			}
			// type 5 (S)
			if(this.type==5)
			{
				// 1
				this.part[1].x=this.part[0].x-1;
				this.part[1].y=this.part[0].y;
				// 2
				this.part[2].x=this.part[0].x;
				this.part[2].y=this.part[0].y-1;
				// 3
				this.part[3].x=this.part[0].x+1;
				this.part[3].y=this.part[0].y-1; 
			}
			// type 6 (Z)
			if(this.type==6)
			{
				// 1
				this.part[1].x=this.part[0].x+1;
				this.part[1].y=this.part[0].y;
				// 2
				this.part[2].x=this.part[0].x;
				this.part[2].y=this.part[0].y-1;
				// 3
				this.part[3].x=this.part[0].x-1;
				this.part[3].y=this.part[0].y-1; 
			}

		}

		setRotation(rotation)
		{
			//set rotation

			this.rotation=rotation;

			// if rotation is 0 do nothing

			// rotation 1 90°
			if(this.rotation==1)
			{
				for(var i=1;i<4;i++)
				{
					var tempX=this.part[i].x;
					var tempY=this.part[i].y;
					this.part[i].x=-tempY;
					this.part[i].y=tempX;
				}
			}
			// rotation 2 180°
			if(this.rotation==2)
			{
				for(var i=1;i<4;i++)
				{
					var tempX=this.part[i].x;
					var tempY=this.part[i].y;
					this.part[i].x=-tempX;
					this.part[i].y=-tempY;	
				}
			}
			// rotation 3 270°
			if(this.rotation==3)
			{
				for(var i=1;i<4;i++)
				{
					var tempX=this.part[i].x;
					var tempY=this.part[i].y;
					this.part[i].x=tempY;
					this.part[i].y=-tempX;
				}
			}
		}

		setPosition(x,y)
		{
			for(var i=0;i<4;i++)
			{
				this.position[i].x=x+this.part[i].x;
				this.position[i].y=y+this.part[i].y;	
			}
		}
	}

	  //////////////////////////////////////////////////////////////////////////////////
	 /////                              GRID CLASS                                /////
	//////////////////////////////////////////////////////////////////////////////////

	class Grid
	{
		constructor()
		{
			this.grid_part=[];
			for(var i=0;i<24;i++)
			{
				this.grid_part[i]=[];
				for(var k=0;k<12;k++)
				{
					this.grid_part[i][k]=new gridPart();
					this.grid_part[i][k].fill=false;
					this.grid_part[i][k].x=k;
					this.grid_part[i][k].y=i;
				}
			}
		}
	}

	  //////////////////////////////////////////////////////////////////////////////////
	 /////                              GAME CLASS                                /////
	//////////////////////////////////////////////////////////////////////////////////

	class Game
	{
		constructor()
		{
			// rotation,position and type
			this.rotation_index=0;
			this.type_index=0;
			this.position_x=6;
			this.position_y=-4; 
			this.score=0;
			this.line_index=[];
			this.on_top=false;
			this.able=true;
			this.able_move=true;
			this.line_full=true;
			this.game_over=false;
			this.turbo_speed=false;
			this.pause=false;

			// grid,piece and size instances
			this.piece_object=new Piece();
			this.grid_object=new Grid(); 
			this.size_object=new gameSize(4);

			//key action 
			this.left=false;
			this.right=false;
			this.space=false;

			// canvas
			this.canvas=document.getElementById("canvas");
			this.ctx=canvas.getContext("2d");
			this.ctx.canvas.width=this.size_object.gameUnit*12;
			this.ctx.canvas.height=this.size_object.gameUnit*28;

			//set initial piece settings
			this.piece_object.setPiece();
		}

		setPiece()
		{
			// rotates piece 
			this.piece_object.setPiece(this.type_index);
			this.piece_object.setRotation(this.rotation_index);	
			this.piece_object.setPosition(this.position_x,this.position_y);
		}

		gameOverTxt()
		{
			this.ctx.fillStyle='black';
			this.ctx.font="bold "+this.size_object.gameUnit*1+"px 'Montserrat'";
			this.ctx.fillText("GAME OVER",this.size_object.gameUnit,this.size_object.gameUnit*11);
		}

		gamePauseTxt()
		{
			this.ctx.fillStyle='black';
			this.ctx.font="bold "+this.size_object.gameUnit*2+"px 'Montserrat'";
			this.ctx.fillText("PAUSE",this.size_object.gameUnit,this.size_object.gameUnit*11);
		}

		drawGame()
		{
			this.clearScrean();
			this.drawPiece();
			this.drawGrid();
			// FOR DEBUG:
			/*
			console.log("rotation_index: "+this.rotation_index)
			for(var i=0;i<4;i++)
			{	
				console.log("position "+i+" x: "+this.piece_object.position[i].x);
				console.log("position "+i+" y: "+this.piece_object.position[i].y);
			}*/
		}

		drawPiece()
		{
			this.ctx.fillStyle='blue';
			for(var i=0;i<4;i++)
				this.ctx.fillRect 
				(
					this.piece_object.position[i].x*this.size_object.gameUnit,
					this.piece_object.position[i].y*this.size_object.gameUnit,
					this.size_object.gameUnit,
					this.size_object.gameUnit
				);
		}

		drawGrid()
		{
			for(var i=0;i<24;i++)
				for(var k=0;k<12;k++)
					if(this.grid_object.grid_part[i][k].fill==true)
					{
						this.ctx.fillStyle='green';
						this.ctx.fillRect
						(
							this.grid_object.grid_part[i][k].x*this.size_object.gameUnit,
							this.grid_object.grid_part[i][k].y*this.size_object.gameUnit,
							this.size_object.gameUnit,
							this.size_object.gameUnit
						)
					}

		}

		clearScrean()
		{
			this.ctx.clearRect(0,0,this.size_object.gameWidth,this.size_object.gameHeight);
			this.ctx.fillStyle='red';
			this.ctx.fillRect(0,0,this.size_object.gameWidth,this.size_object.gameWidth*2);
			this.ctx.fillStyle='black';
			this.ctx.font="bold "+this.size_object.gameUnit*1+"px 'Montserrat'";
			this.ctx.fillText("SCORE "+this.score,this.size_object.gameUnit,this.size_object.gameUnit*25);
		}

		gameScore()
		{
			for(var i=0;i<24;i++)
			{	
				this.line_full=true;			
				for(var k=0;k<12;k++)
				{
					if(this.grid_object.grid_part[i][k].fill!=true)
						this.line_full=false;
				if(this.line_full)
					this.line_index[i]=i;
				else this.line_index[i]=-1;
				}
			}
			for(var j=0;j<24;j++)
				if(this.line_index[j]!=-1)
				{
					this.score+=10;
					for(var i=this.line_index[j];i>0;i--)
						for(var k=0;k<12;k++)
						{
							this.grid_object.grid_part[i][k].fill=this.grid_object.grid_part[i-1][k].fill;
						}
				}
		}

		// random type random rotation
		gameRand()
		{
			this.type_index=Math.floor(Math.random()*7);
			this.rotation_index=Math.floor(Math.random()*4);
			// FOR DEBUG:
			//console.log("GAME RAND");
			//console.log(this.type_index);
			//console.log(this.rotation_index);
		}

		gameRotation()
		{
			this.able=true;
			this.rotation_index++;
		 		if(this.rotation_index==4)
		 			this.rotation_index=0;
		 	this.setPiece();

		 	
		 	for(var i=0;i<24;i++)
					for(var k=0;k<12;k++)
						for(var l=0;l<4;l++)
						{
							if
							(
								this.grid_object.grid_part[i][k].x==this.piece_object.position[l].x
								&&this.grid_object.grid_part[i][k].y==this.piece_object.position[l].y
								&&this.grid_object.grid_part[i][k].fill==true
							)
							{
								this.able=false;
								//console.log("BLOCKED GRID"); // DEBUG
							}
							if
							(
								this.piece_object.position[l].x<0
								||this.piece_object.position[l].x>11
								||this.piece_object.position[l].y>23
							)
							{
								this.able=false;
								//console.log("BLOCKED WALL"); // DEBUG
							}
						}
			if(!this.able)
			{
				//console.log("RETURNING BACK") // DEBUG
				this.rotation_index--;
				if(this.rotation_index<0)
					this.rotation_index=3;
				this.setPiece();
			}
		}

		gameLogic()
		{
			// Here y position of piece increments
			// or if it is on max position it returns to
			// min position.
			this.on_top=false;

			if
			(
				this.piece_object.position[0].y>=23//<-bottom border
				||this.piece_object.position[1].y>=23
				||this.piece_object.position[2].y>=23
				||this.piece_object.position[3].y>=23
			)
			{
				for(var i=0;i<24;i++)
					for(var k=0;k<12;k++)
						for(var l=0;l<4;l++)
							if
							(
								this.grid_object.grid_part[i][k].x==this.piece_object.position[l].x
								&&this.grid_object.grid_part[i][k].y==this.piece_object.position[l].y
							)
							{
								this.grid_object.grid_part[i][k].fill=true;
							}
				this.position_y=-4;
				this.position_x=6;
				this.turbo_speed=false;
				this.gameRand();
			}

			else
			{
				for(var i=0;i<24;i++)
					for(var k=0;k<12;k++)
						for(var l=0;l<4;l++)
						{
							if
							(
								this.grid_object.grid_part[i][k].x==this.piece_object.position[l].x
								&&this.grid_object.grid_part[i][k].y==(this.piece_object.position[l].y+1)
								&&this.grid_object.grid_part[i][k].fill==true
							)
							{
								this.on_top=true;
							}
						}
				if(this.on_top)
				{
					for(var i=0;i<24;i++)
						for(var k=0;k<12;k++)
							for(var l=0;l<4;l++)
								if
								(
									this.grid_object.grid_part[i][k].x==this.piece_object.position[l].x
									&&this.grid_object.grid_part[i][k].y==this.piece_object.position[l].y
								)
								{
									this.grid_object.grid_part[i][k].fill=true;
								}
					this.position_y=-4;
					this.position_x=6;
					this.turbo_speed=false;
					this.gameRand();
				}
				else
				{
					this.position_y++;
				}			
			}
		}

		gameOver()
		{
			for(var k=0;k<12;k++)
				if(this.grid_object.grid_part[3][k].fill==true)
					this.game_over=true;
		}

		keyAction() //if key is pressed do action 
		{
			this.able_move=true;
			if(this.left==true
				&&this.piece_object.position[0].x>0//<-left border
				&&this.piece_object.position[1].x>0
				&&this.piece_object.position[2].x>0
				&&this.piece_object.position[3].x>0
			)
			{
				this.position_x--;
				this.setPiece();
				for(var i=0;i<24;i++)
					for(var k=0;k<12;k++)
						for(var l=0;l<4;l++)
						{
							if
							(
								this.grid_object.grid_part[i][k].x==this.piece_object.position[l].x
								&&this.grid_object.grid_part[i][k].y==this.piece_object.position[l].y
								&&this.grid_object.grid_part[i][k].fill==true
							)
							{
								this.able_move=false;
								//console.log("MOVE BLOCKED GRID");
							}
						}
				if(!this.able_move)
				{	
					this.position_x++;
					this.setPiece();
					this.left=false;
				}
				else
				{
					this.left=false;
				}	
			}
			if(this.right==true
				&&this.piece_object.position[0].x<11//<-right border
				&&this.piece_object.position[1].x<11
				&&this.piece_object.position[2].x<11
				&&this.piece_object.position[3].x<11
			)
			{
				this.position_x++;
				this.setPiece();
				for(var i=0;i<24;i++)
					for(var k=0;k<12;k++)
						for(var l=0;l<4;l++)
						{
							if
							(
								this.grid_object.grid_part[i][k].x==this.piece_object.position[l].x
								&&this.grid_object.grid_part[i][k].y==this.piece_object.position[l].y
								&&this.grid_object.grid_part[i][k].fill==true
							)
							{
								this.able_move=false;
								//console.log("MOVE BLOCKED GRID");
							}
						}
				if(!this.able_move)
				{	
					this.position_x--;
					this.setPiece();
					this.right=false;
				}
				else
				{
					this.right=false;
				}	
			}
			if(this.space==true)
			{
				this.gameRotation();
		 		this.space=false;
			}

		}
	}

	  //////////////////////////////////////////////////////////////////////////////////
	 /////                              MAIN FUNCTION                             /////
	//////////////////////////////////////////////////////////////////////////////////

	// main function is able to run only after full page is loaded.
	window.onload=(function()// BEGINING OF MAIN FUNCTION
	{
		
		// game object
		var game_object=new Game();


		// getting keycode from browser
		// NOT WORKING WHEN MEMBER OF GAME CLASS
 		window.addEventListener ("keydown",getKey,false);

		// NOT WORKING WHEN MEMBER OF GAME CLASS
		function getKey(key)
		{
			// KEYCODES:
			// UP 38 
			// DOWN 40
			// LEFT 37
			// RIGHT 39
			// SPACE 32
			// ENTER 13

				//if LEFT is pressed
				if(key.keyCode==37)
				{
					game_object.left=true;
					game_object.right=false;
				}
					
				//if RIGHT is pressed
				if(key.keyCode==39)
				{
					game_object.right=true;
					game_object.left=false;
				}

				//if SPACE is pressed
	 			if(key.keyCode==32)
	 				game_object.space=true;

	 			//if ENTER is pressed
	 			if(key.keyCode==13)
	 				game_object=new Game();

	 			//if DOWN is pressed
	 			if(key.keyCode==40)
	 			{
	 				game_object.turbo_speed=true;
	 				// DEBUG:
	 				//console.log("TURBO ON")
	 			}

	 			//if UP is pressed
	 			if(key.keyCode==38)
	 			{
	 				if(!game_object.pause)
	 					game_object.pause=true;
	 				else
	 					game_object.pause=false;
	 			}

		}

 		function gameFunc1()// renders game and calls game functions
 		{
 			if(!game_object.game_over&&!game_object.pause)
 			{
 				game_object.gameOver();		
	 			game_object.keyAction();
	 			game_object.setPiece();
	 			game_object.drawGame();
	 			game_object.gameScore();
 			}

 			if(game_object.game_over)
 			{
 				game_object.gameOverTxt();
 			}

 			if(game_object.pause)
 			{
 				game_object.gamePauseTxt();
 			}
 		}

 		function gameFunc2()// increments piece position
 		{
 			if(!game_object.game_over&&!game_object.turbo_speed&&!game_object.pause)
 			{ 				
 				game_object.gameLogic();
 				// DEBUG:
 				//console.log("NORMAL GAME SPEED")
				//console.log(game_object.size_object.gameUnit)
 			}	
 		}

 		function gameFunc3()// increments piece position
 		{
 			if(!game_object.game_over&&game_object.turbo_speed&&!game_object.pause)
 			{ 				
 				game_object.gameLogic();
 				// DEBUG:
 				//console.log("TURBO SPEED")
 			}	
 		}


		//periodicaly calling gameFunc1
		setInterval(function(){gameFunc1();},10);

		//periodicaly calling gameFunc2 
		setInterval(function(){gameFunc2();},300);

		//periodicaly calling gameFunc3 
		setInterval(function(){gameFunc3();},50);

	})();// END OF MAIN FUNCTION



//END OF PRIVATE SCOPE
})();